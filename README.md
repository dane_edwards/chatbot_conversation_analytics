# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* PowerBI report for chatbot analytics
* 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* You will need a SQL database with converstion chatbot data
* Use the script in "create table" to create a table in SQL 
* You will need PowerBI, the R extension and the Word Cloud extension
* Open PowerBI report and connect to data source with the schema that is in the "create table" script
* Added "Cluster Strings.R" for a standalone R version of the clustering

